//Kausar Mussa
//1738212
package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	public LowercaseProcessor (String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
		
	}

	public ArrayList <String> process ( ArrayList <String> toChange) {
		

		 ArrayList <String> asLower = new ArrayList <String>();
		String toAdd;
		
		for ( int i = 0; i< toChange.size() ; i++) {
			toAdd = toChange.get(i).toLowerCase();
			asLower.add(toAdd);
		//	System.out.println(asLower.get(i));
		}
		return asLower;
	} 

}
