//Kausar Mussa
//1738212
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {

	public RemoveDuplicates (String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
		
	}
	
	public ArrayList <String> process ( ArrayList <String> toRemove) {
		ArrayList <String> nodupli = new ArrayList <String> ();
		
		for(int i = 0; i< toRemove.size(); i++) {
			if (! nodupli.contains(toRemove.get(i))) {
				nodupli.add(toRemove.get(i));	
			}
			
			
		}
		
		return nodupli;
		
	}
}
